package library.dao;

import library.ConnectDB;
import library.model.Author;
import library.model.Slogan;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class SloganDAO {

	Connection connection = ConnectDB.getDBConnection();

	private ResultSet getResultSet() throws SQLException {
		Statement statement = connection.createStatement();
		statement.setQueryTimeout(30);
		return statement.executeQuery("SELECT * FROM slogan");
	}

	public String getSlogan() {

		List<String> slogans = new ArrayList<>();

		try {
			ResultSet rs = getResultSet();
			while (rs.next()) {
				slogans.add(rs.getString("slogan"));
			}
		} catch (SQLException e) {
			throw new RuntimeException("Error query! (method getCategories()", e);
		}

		int index = 0;

		return slogans.get(index);
	}
}
