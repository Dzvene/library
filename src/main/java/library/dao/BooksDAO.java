package library.dao;

import library.ConnectDB;
import library.model.Book;
import library.model.Genre;

import javax.swing.*;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class BooksDAO {

	private Book book = null;
	private Connection connection = ConnectDB.getDBConnection();
	private List<Book> bookList = new ArrayList<>();
	private List<Book> bookListGenre = new ArrayList<>();

	private ResultSet getResultSet() throws SQLException {
		Statement statement = connection.createStatement();
		statement.setQueryTimeout(30);
		return statement.executeQuery("SELECT * FROM books");
	}

	public List<Book> getBook(){

		try {
			ResultSet rs = getResultSet();

			while (rs.next()) {
				createBookObject(rs);
				bookList.add(book);
			}

			return bookList;

		} catch (SQLException e) {
			throw new RuntimeException("Error query! (method getBook()", e);
		}
	}

	public List<Book> getBookGenre(int index) {

		try {
			Statement statement = connection.createStatement();
			statement.setQueryTimeout(30);
			ResultSet rs = statement.executeQuery("SELECT * FROM books WHERE genre_id = " + index);

			while (rs.next()) {
				createBookObject(rs);
				bookListGenre.add(book);
			}
		} catch (SQLException e) {
			throw new RuntimeException("Error query! (method get() ", e);
		}
		return bookListGenre;
	}

	public List<Book> getBookLetter(String letter) {

		try {
			Statement statement = connection.createStatement();
			statement.setQueryTimeout(30);
			ResultSet rs = statement.executeQuery("SELECT * FROM books WHERE name LIKE '" + letter + "%'");
			while (rs.next()) {
				createBookObject(rs);
				bookListGenre.add(book);
			}
		} catch (SQLException e) {
			throw new RuntimeException("Error query! (method get() ", e);
		}
		return bookListGenre;
	}

	public List<Book> getBookSearch(String search) {

		try {
			Statement statement = connection.createStatement();
			statement.setQueryTimeout(30);
			ResultSet rs = statement.executeQuery("SELECT * FROM books WHERE name LIKE '%" + search + "%'");
			while (rs.next()) {
				createBookObject(rs);
				bookListGenre.add(book);
			}
		} catch (SQLException e) {
			throw new RuntimeException("Error query! (method get() ", e);
		}
		return bookListGenre;
	}

	public Book get(int index) {

		try {
			Statement statement = connection.createStatement();
			statement.setQueryTimeout(30);
			ResultSet rs = statement.executeQuery("SELECT * FROM books WHERE id = " + index);
			while (rs.next()) {
				createBookObject(rs);
			}
		} catch (SQLException e) {
			throw new RuntimeException("Error query! (method get() ", e);
		}
		return book;
	}

	private void createBookObject(ResultSet rs) throws SQLException {
		book = new Book(
				rs.getInt("id"),
				rs.getString("name"),
				rs.getString("content"),
				rs.getInt("page_count"),
				rs.getString("isbn"),
				rs.getString("genre_id"),
				rs.getString("autor_id"),
//						rs.getDate("publish-year"),
				rs.getString("publisher_id"),
				rs.getString("image")
		);
	}
}
