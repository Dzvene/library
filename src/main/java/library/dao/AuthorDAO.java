package library.dao;

import library.ConnectDB;
import library.model.Author;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class AuthorDAO {

	private Author author = null;
	private Connection connection = ConnectDB.getDBConnection();
	private List<Author> authors = new ArrayList<>();

	private ResultSet getResultSet() throws SQLException {
		Statement statement = connection.createStatement();
		statement.setQueryTimeout(30);
		return statement.executeQuery("SELECT * FROM author");
	}

	public List<Author> getAuthor(){

		try {
			ResultSet rs = getResultSet();

			while (rs.next()) {
				authors.add(new Author(rs.getInt("id"), rs.getString("name")));
			}
			return authors;
		} catch (SQLException e) {
			throw new RuntimeException("Error query! (method getName()", e);
		}
	}

	public Author get(int index) {

		try {
			Statement statement = connection.createStatement();
			statement.setQueryTimeout(30);
			ResultSet rs = statement.executeQuery("SELECT * FROM author WHERE ID = " + index);
			while (rs.next()) {
				author = new Author(rs.getInt("id"), rs.getString("name"));
			}
		} catch (SQLException e) {
			throw new RuntimeException("Error query! (method get() ", e);
		}
		return author;
	}


}
