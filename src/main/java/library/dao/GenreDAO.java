package library.dao;

import library.ConnectDB;
import library.model.Genre;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class GenreDAO {

	private Genre genre = null;
	private Connection connection = ConnectDB.getDBConnection();
	private List<Genre> genres = new ArrayList<>();

	private ResultSet getResultSet() throws SQLException {
		Statement statement = connection.createStatement();
		statement.setQueryTimeout(30);
		return statement.executeQuery("SELECT * FROM genre");
	}

	public List<Genre> getGanre(){

		try {
			ResultSet rs = getResultSet();

			while (rs.next()) {
				genres.add(new Genre(rs.getInt("id"), rs.getString("name")));
			}
			return genres;
		} catch (SQLException e) {
			throw new RuntimeException("Error query! (method getName()", e);
		}
	}

	public Genre get(int index) {

		try {
			Statement statement = connection.createStatement();
			statement.setQueryTimeout(30);
			ResultSet rs = statement.executeQuery("SELECT * FROM author WHERE ID = " + index);
			while (rs.next()) {
				genre = new Genre(rs.getInt("id"), rs.getString("name"));
			}
		} catch (SQLException e) {
			throw new RuntimeException("Error query! (method get() ", e);
		}
		return genre;
	}
}
