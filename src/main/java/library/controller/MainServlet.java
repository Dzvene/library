package library.controller;

import library.dao.AuthorDAO;
import library.dao.BooksDAO;
import library.dao.GenreDAO;
import library.dao.SloganDAO;
import library.model.LettersList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class MainServlet extends HttpServlet {

	private HttpSession session;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String action = getAction(request);
		session = request.getSession();


		session.setAttribute("username", request.getParameter("login"));
		System.out.println(request.getParameter("login"));

//		if (request.getParameter("login") != null){
////			session.setAttribute("login", request.getParameter("login"));
//			request.setAttribute("login", request.getParameter("login"));
//		}



		SloganDAO sloganDAO = new SloganDAO();
		AuthorDAO authorDAO = new AuthorDAO();
		GenreDAO genreDAO = new GenreDAO();
		BooksDAO booksDAO = new BooksDAO();
		LettersList letterList = new LettersList();

		request.setAttribute("slogan", sloganDAO.getSlogan());
		request.setAttribute("author",  authorDAO.getAuthor());
		request.setAttribute("genres", genreDAO.getGanre());
		request.setAttribute("letterList", letterList.getRussianLetters());




		switch (action) {
			case "/":
				request.setAttribute("books", booksDAO.getBook());

				request.getRequestDispatcher("/index.jsp").forward(request, response);
				break;

			case "/registration":
				request.getRequestDispatcher("/pages/registration.jsp").forward(request, response);
				break;

			case "/books":

				if (request.getParameter("genre_id") != null){
					request.setAttribute("booksGenre", booksDAO.getBookGenre(Integer.valueOf(request.getParameter("genre_id"))));
					request.setAttribute("active", request.getParameter("genre_id"));
				}else if (request.getParameter("letter") != null){
					request.setAttribute("booksGenre", booksDAO.getBookLetter(request.getParameter("letter")));
					/*todo: для списка букв сделать активный элемент*/
					request.setAttribute("active_letter", request.getParameter("letter"));
				}else if (request.getParameter("search_books") != null) {
					request.setAttribute("booksGenre", booksDAO.getBookSearch(request.getParameter("search_books")));
				}



				request.getRequestDispatcher("/pages/books.jsp").forward(request, response);
				break;

			case "/book":
				request.getRequestDispatcher("/pages/book.jsp").forward(request, response);
				break;
		}
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	}

	private String getAction(HttpServletRequest request) {
		String requestUrl = request.getPathInfo();
		return requestUrl.substring(request.getContextPath().length(), requestUrl.length());
	}
}

