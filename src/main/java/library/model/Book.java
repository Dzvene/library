package library.model;

import java.util.Date;

public class Book {

	private int id;
	private String name;
	private String content;
	private int pageCount;
	private String isbn;
	private String genre;
	private String author;
	private Date publisherYear;
	private String publisher;
	private String image;
	private Genre genreObj;


	public Book(int id, String name, String content, int pageCount, String isbn, String genre, String author, String publisher, String image) {
		this.id = id;
		this.name = name;
		this.content = content;
		this.pageCount = pageCount;
		this.isbn = isbn;
		this.genre = genre;
		this.author = author;
//		this.publisherYear = publisherYear;
		this.publisher = publisher;
		this.image = image;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public int getPageCount() {
		return pageCount;
	}

	public void setPageCount(int pageCount) {
		this.pageCount = pageCount;
	}

	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	public String getGenre() {
		return genre;
	}

	public void setGenre(String genre) {
		this.genre = genre;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public Date getPublisherYear() {
		return publisherYear;
	}

	public void setPublisherYear(Date publisherYear) {
		this.publisherYear = publisherYear;
	}

	public String getPublisher() {
		return publisher;
	}

	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public Genre getGenreObj() {
		return genreObj;
	}

	public void setGenreObj(Genre genreObj) {
		this.genreObj = genreObj;
	}
}

