package library.model;

public class Slogan {

	private int id;
	private String slogan;

	public Slogan(int id, String slogan) {
		this.id = id;
		this.slogan = slogan;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getSlogan() {
		return slogan;
	}

	public void setSlogan(String slogan) {
		this.slogan = slogan;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Slogan author1 = (Slogan) o;

		if (id != author1.id) return false;
		return slogan != null ? slogan.equals(author1.slogan) : author1.slogan == null;

	}

	@Override
	public int hashCode() {
		int result = id;
		result = 31 * result + (slogan != null ? slogan.hashCode() : 0);
		return result;
	}
}

