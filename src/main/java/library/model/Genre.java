package library.model;

public class Genre {

	private int id;
	private String name;

	public Genre(int id, String author) {
		this.id = id;
		this.name = author;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Genre author1 = (Genre) o;

		if (id != author1.id) return false;
		return name != null ? name.equals(author1.name) : author1.name == null;

	}

	@Override
	public int hashCode() {
		int result = id;
		result = 31 * result + (name != null ? name.hashCode() : 0);
		return result;
	}
}

