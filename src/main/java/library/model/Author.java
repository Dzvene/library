package library.model;

public class Author {

	private int id;
	private String author;

	public Author(int id, String author) {
		this.id = id;
		this.author = author;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Author author1 = (Author) o;

		if (id != author1.id) return false;
		return author != null ? author.equals(author1.author) : author1.author == null;

	}

	@Override
	public int hashCode() {
		int result = id;
		result = 31 * result + (author != null ? author.hashCode() : 0);
		return result;
	}
}

