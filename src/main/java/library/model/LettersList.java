package library.model;

public class LettersList {

	public char[] getRussianLetters(){
		char[] letters = new char[33];
		letters[0]='а';
		letters[1]='б';
		letters[2]='в';
		letters[3]='г';
		letters[4]='д';
		letters[5]='е';
		letters[6]='ё';
		letters[7]='ж';
		letters[8]='з';
		letters[9]='и';
		letters[10]='й';
		letters[11]='к';
		letters[12]='л';
		letters[13]='м';
		letters[14]='н';
		letters[15]='о';
		letters[16]='п';
		letters[17]='р';
		letters[18]='с';
		letters[19]='т';
		letters[20]='у';
		letters[21]='Ф';
		letters[22]='ч';
		letters[23]='ц';
		letters[24]='ч';
		letters[25]='ш';
		letters[26]='щ';
		letters[27]='ъ';
		letters[28]='ы';
		letters[29]='ь';
		letters[30]='э';
		letters[31]='ю';
		letters[32]='я';

		return letters;
	}
}
