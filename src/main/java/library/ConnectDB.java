package library;

import java.net.URL;
import java.sql.*;

public class ConnectDB {

	private static final URL url = ConnectDB.class.getClassLoader().getResource("library.db");
	private static final String dbPath = url.getPath();
	private static final String DB_CONNECTION = "jdbc:sqlite:" + dbPath;
	private static final String DB_DRIVER = "org.sqlite.JDBC";
	private static final String DB_USER = "";
	private static final String DB_PASSWORD = "";

	private static Connection dbConnection = null;

	public static Connection getDBConnection() {

		try {
			Class.forName(DB_DRIVER);
		} catch (ClassNotFoundException e) {
			System.out.println(e.getMessage());
		}
		try {
			dbConnection = DriverManager.getConnection(DB_CONNECTION, DB_USER, DB_PASSWORD);
			return dbConnection;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		return dbConnection;
	}
}



