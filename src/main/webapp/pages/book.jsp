<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@include file="../jspf/header.jspf" %>

<section class="main_block cf">
	<div class="left_sidebar f_left">

		<%@include file="../jspf/left_menu.jspf" %>

	</div>

	<div class="content f_left">

			<div>
				<c:out value="${books.id}"/>
				<br>
				<c:out value="${books.name}"/>
				<br>
				<c:out value="${books.pageCount}"/>
				<br>
				<c:out value="${books.isbn}"/>
				<br>
				<c:out value="${books.genreObj}"/>
				<br>
				<c:out value="${books.author}"/>
				<br>
				<img src="<c:out value='${books.image}'/>" alt="${books.name}">

			</div>


		</c:forEach>


	</div>

</section>

<%@include file="../jspf/footer.jspf" %>
