<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@include file="../jspf/header.jspf" %>

<section class="main_block cf">

	<div class="left_sidebar f_left">
		<%@include file="../jspf/left_menu.jspf" %>
	</div>

	<div class="content f_left">
		<div class="books_wrap cf">



			<c:forEach items="${booksGenre}" var="booksGenre" varStatus="loop">
				<div class="book_block f_left">
					<h3><c:out value="${booksGenre.name}"/></h3>
					<div class="image_block">
						<img src="<c:out value='${booksGenre.image}'/>" alt="${booksGenre.name}">
					</div>
					<p><span>ISBN: </span><c:out value="${booksGenre.isbn}"/></p>
					<p><span>Жанр: </span><c:out value="${booksGenre.genre}"/></p>
					<p><span>Количество страниц: </span><c:out value="${booksGenre.pageCount}"/></p>
					<p><span>Автор: </span><c:out value="${booksGenre.author}"/></p>
					<a href="#">Читать</a>
				</div>
			</c:forEach>

			<%--<c:if test="${booksGenre == 0}">--%>
				<%--Ничего не найдено!--%>
			<%--</c:if>--%>



		</div>
	</div>

</section>

<%@include file="../jspf/footer.jspf" %>
