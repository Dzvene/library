<%@ page import="library.model.Book" %>
<%@ page import="java.util.ArrayList" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@include file="jspf/header.jspf" %>

<section class="main_block cf">
	<div class="left_sidebar f_left">

		<%@include file="jspf/left_menu.jspf" %>

	</div>
	<div class="content f_left">

		<div class="books_wrap cf">
			<c:forEach items="${books}" var="books" varStatus="loop">
				<div class="book_block f_left">
					<h3><c:out value="${books.name}"/></h3>
					<div class="image_block">
						<img src="<c:out value='${books.image}'/>" alt="${books.name}">
					</div>
					<p><span>ISBN: </span><c:out value="${books.isbn}"/></p>
					<p><span>Жанр: </span><c:out value="${books.genre}"/></p>
					<p><span>Количество страниц: </span><c:out value="${books.pageCount}"/></p>
					<p><span>Автор: </span><c:out value="${books.author}"/></p>
					<a href="#">Читать</a>
				</div>
			</c:forEach>
		</div>


	</div>

</section>

<%@include file="jspf/footer.jspf" %>
